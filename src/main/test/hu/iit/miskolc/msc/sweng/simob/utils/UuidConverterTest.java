package hu.iit.miskolc.msc.sweng.simob.utils;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UuidConverterTest {

    @Test
    public void parse() {
        String uuidString = "8ac25467-ac6c-43fe-a661-4aab368d528c";
        UUID uuid = UuidConverter.parse(uuidString);
        assertEquals(uuidString, uuid.toString());
    }

    @Test
    public void print() {
        UUID uuid = UUID.fromString("8ac25467-ac6c-43fe-a661-4aab368d528c");
        String uuidString = "8ac25467-ac6c-43fe-a661-4aab368d528c";
        assertEquals(uuidString, UuidConverter.print(uuid));
    }

}