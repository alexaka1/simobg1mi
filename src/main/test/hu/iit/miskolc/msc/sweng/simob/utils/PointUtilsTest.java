package hu.iit.miskolc.msc.sweng.simob.utils;

import hu.iit.miskolc.msc.sweng.simob.model.Point;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointUtilsTest {
    private Point point1;
    private Point point2;
    private Point point3;

    @BeforeEach
    void setUp() {
        point1 = new Point();
        point1.setX(5);
        point1.setY(10);
        point1.setId(234);
        point2 = new Point();
        point2.setX(7);
        point2.setY(213);
        point2.setId(1);
        point3 = new Point();
        point3.setX(98);
        point3.setY(12);
        point3.setId(2);
    }

    @Test
    void distanceFromLine() {
        Double actual = PointUtils.distanceFromLine(point1, point2, point3);
        Double expected = 85.54660016795405;
        assertEquals(actual, expected, 0.0001);
    }

    @Test
    void isBetweenHorizontally() {
        assertFalse(PointUtils.isBetweenHorizontally(point1, point2, point3));
        point1.setX(1);
        point1.setY(1);
        point2.setX(1);
        point2.setY(2);
        point3.setX(1);
        point3.setY(3);
        assertTrue(PointUtils.isBetweenHorizontally(point1, point2, point3));
    }

    @Test
    void isBetweenVertically() {
        assertFalse(PointUtils.isBetweenVertically(point1, point2, point3));
        point1.setX(1);
        point1.setY(1);
        point2.setX(2);
        point2.setY(1);
        point3.setX(3);
        point3.setY(1);
        assertTrue(PointUtils.isBetweenVertically(point1, point2, point3));
    }

    @Test
    void isBetween() {
        assertFalse(PointUtils.isBetween(point1, point2, point3));
        point1.setX(1);
        point1.setY(1);
        point2.setX(2);
        point2.setY(2);
        point3.setX(2);
        point3.setY(3);
        assertTrue(PointUtils.isBetween(point2, point1, point3));
    }
}