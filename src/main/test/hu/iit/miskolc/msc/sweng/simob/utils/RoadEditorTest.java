package hu.iit.miskolc.msc.sweng.simob.utils;

import hu.iit.miskolc.msc.sweng.simob.exceptions.UnsavedException;
import hu.iit.miskolc.msc.sweng.simob.model.Road;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RoadEditorTest {

    private RoadEditor roadEditor;

    @BeforeEach
    public void setUp() throws JAXBException {
        roadEditor = new RoadEditor();
    }

    @Test
    public void getInstance() {
        assertNotNull(roadEditor);
    }

    @Test
    public void createRoad() {
        assertNotNull(roadEditor.createRoad());
    }

    @Test
    public void createRoads() throws JAXBException, UnsavedException {
        assertNotNull(roadEditor.createRoads());
    }

    @Test
    public void getRoads() throws JAXBException, UnsavedException {
        roadEditor.createRoads();
        List<Road> roadList = roadEditor.getRoads();
        assertNotNull(roadList);
    }

    @Test
    public void getRoad() {
        Road road = new Road();
        road.setWidth(1);
        road.setName("testRoad");
        road.setLength(21);
        road.setNumberOfLanes(2);
        road.setDirection("biderectional");
        road.setRoadId(UUID.randomUUID());
        roadEditor.setRoad(road);

        Road actualRoad = roadEditor.getRoad();
        assertEquals(road, actualRoad);
    }
}