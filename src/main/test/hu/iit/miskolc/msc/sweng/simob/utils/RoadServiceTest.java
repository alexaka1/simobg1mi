package hu.iit.miskolc.msc.sweng.simob.utils;

import hu.iit.miskolc.msc.sweng.simob.model.Road;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RoadServiceTest {
    private RoadService roadService;
    private String filename;

    @BeforeEach
    public void setUp() throws JAXBException, IOException {
        roadService = new RoadService();
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("test.xml").getFile());
        filename = file.getAbsolutePath();

    }

    @Test
    public void createRoad() {
        Road road = roadService.createRoad();
        assertNotNull(road);
        assertNotNull(road.getRoadId());
    }

    @Test
    public void createRoads() {
        List<Road> roads = roadService.createRoads();
        assertNotNull(roads);
        assertEquals(0, roads.size());
    }

    @Test
    public void store() throws JAXBException {
        List<Road> roads = new ArrayList<>();
        roads.add(new Road());
        roadService.store(roads, filename);
        assertNotNull(new File(filename));
    }

    @Test
    public void load() throws JAXBException {
        List<Road> roads = roadService.load(filename);
        assertNotNull(roads);
    }
}