package hu.iit.miskolc.msc.sweng.simob.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public class ConfigurationHandler {

	// TODO: i think it shouldn't be hardcoded
	private static final String CONFIG_PROPERTIES = "src/main/resources/config.properties";

	private static ConfigurationHandler instance;

	private final Properties persistedProperties;
	private final Properties runtimeProperties;

	private ConfigurationHandler() {
		persistedProperties = new Properties();
		runtimeProperties = new Properties();

		try {
			persistedProperties.load(new FileInputStream(CONFIG_PROPERTIES));
			runtimeProperties.load(new FileInputStream(CONFIG_PROPERTIES));
		} catch (IOException e) {
			// TODO: use logger, maybe better exception handling
			System.err.println("Failed to load configuration. Continue with defaults.");
		}
	}

	/**
	 * Return singleton instance of the configuration handler class.
	 * 
	 * @return Singleton instance of the configuration handler class.
	 */
	public static ConfigurationHandler getInstance() {
		if (instance == null) {
			instance = new ConfigurationHandler();
		}
		return instance;
	}

	/**
	 * Get string value of the given property.
	 * 
	 * @param key
	 * @return String value of the given property.
	 */
	public String get(String key) {
		return runtimeProperties.getProperty(key);
	}

	/**
	 * Get value of property in the default value's type.
	 * 
	 * @param key
	 * @param defaultValue
	 * @return Value of property in the default value's type or the default value
	 *         itself.
	 */
	@SuppressWarnings("unchecked")
	private <T> T get(String key, T defaultValue) {
		String stringValue = get(key);

		if (stringValue == null) {
			return defaultValue;
		}

		try {
			Constructor<T> constructor = (Constructor<T>) defaultValue.getClass().getConstructor(String.class);

			constructor.setAccessible(true);

			return constructor.newInstance(stringValue);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			// it will never happen
			return defaultValue;
		}
	}

	/**
	 * Get string value of the given property.
	 * 
	 * @param key
	 * @param defaultValue
	 * @return String value of the given property or the default in case of
	 *         problems.
	 */
	public String get(String key, String defaultValue) {
		return (String) get(key, (Object) defaultValue);
	}

	@SuppressWarnings("unchecked")
	public <T extends Number> T get(String key, T defaultValue) {
		return (T) get(key, (Object) defaultValue);
	}

	/**
	 * Get boolean value of the given property.
	 * 
	 * @param key
	 * @param defaultValue
	 * @return Boolean value of the property or the default in case of problems.
	 */
	public Boolean get(String key, Boolean defaultValue) {
		return (Boolean) get(key, (Object) defaultValue);
	}

	/**
	 * Check whether the given property is persisted.
	 * 
	 * @param key
	 * @return Whether the given property is persisted.
	 */
	public boolean isPersisted(String key) {
		return persistedProperties.containsKey(key);
	}

	/**
	 * Set property.
	 * 
	 * @param key   Key value of the property.
	 * @param value Actual value of the property.
	 */
	public <T> void set(String key, T value) {
		runtimeProperties.setProperty(key, String.valueOf(value));

		if (isPersisted(key)) {
			persistedProperties.setProperty(key, String.valueOf(value));
			save();
		}
	}

	/**
	 * Save properties that need to be persisted.
	 */
	private void save() {
		try {
			persistedProperties.store(new FileOutputStream(CONFIG_PROPERTIES), null);
		} catch (IOException e) {
			// TODO: use logger, maybe better exception handling
			System.err.println("Failed to save configuration.");
		}
	}

}
