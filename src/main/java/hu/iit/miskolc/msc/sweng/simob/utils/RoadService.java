package hu.iit.miskolc.msc.sweng.simob.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import hu.iit.miskolc.msc.sweng.simob.model.ObjectFactory;
import hu.iit.miskolc.msc.sweng.simob.model.Road;

public class RoadService {

	private final ObjectFactory objectFactory;
	private final Marshaller marshaller;
	private final Unmarshaller unmarshaller;

	public RoadService() throws JAXBException {
		objectFactory = new ObjectFactory();

		JAXBContext jaxbContext = JAXBContext.newInstance(Road[].class);

		marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		unmarshaller = jaxbContext.createUnmarshaller();
	}

	/**
	 * Create new road and set its ID.
	 * 
	 * @return New road.
	 */
	public Road createRoad() {
		Road road = objectFactory.createRoad();

		road.setRoadId(UUID.randomUUID());

		return road;
	}

	/**
	 * Create empty road list.
	 * 
	 * @return Empty road list.
	 */
	public List<Road> createRoads() {
		return new ArrayList<>();
	}

	/**
	 * Save the given road list in the given file.
	 * 
	 * @param roads    Road list to be saved.
	 * @param fileName File to save.
	 * @throws JAXBException If the data can't be parsed.
	 */
	public void store(List<Road> roads, String fileName) throws JAXBException {
		JAXBElement<Road[]> root = new JAXBElement<Road[]>(new QName("roads"), Road[].class,
				roads.toArray(new Road[roads.size()]));
		marshaller.marshal(root, new File(fileName));
	}

	/**
	 * Load road network from the given file.
	 * 
	 * @param fileName Name of file that contains the road network.
	 * @return List of roads loaded from the given file.
	 * @throws JAXBException If the file can't be parsed based on our schema.
	 */
	public List<Road> load(String fileName) throws JAXBException {
		// TODO: better exception handling
		XMLStreamReader xmlStreamReader = null;
		try {
			xmlStreamReader = XMLInputFactory.newInstance()
					.createXMLStreamReader(new FileInputStream(new File(fileName)));
		} catch (FileNotFoundException e) {
		} catch (XMLStreamException e) {
		} catch (FactoryConfigurationError e) {
		}

		List<Road> roads = new ArrayList<>();
		if (xmlStreamReader != null) {
			Arrays.stream(unmarshaller.unmarshal(xmlStreamReader, Road[].class).getValue()).forEach(roads::add);
		}

		return roads;
	}

}
