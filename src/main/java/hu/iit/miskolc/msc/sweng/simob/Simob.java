package hu.iit.miskolc.msc.sweng.simob;

import javax.swing.SwingUtilities;

import hu.iit.miskolc.msc.sweng.simob.frames.MainFrame;

public class Simob implements Runnable {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Simob());
	}

	@Override
	public void run() {
		new MainFrame();
	}
}
