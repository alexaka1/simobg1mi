package hu.iit.miskolc.msc.sweng.simob.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JCheckBox;

import hu.iit.miskolc.msc.sweng.simob.utils.ConfigurationHandler;

public class WelcomeScreenCheckBoxAction extends AbstractAction {

	private static final long serialVersionUID = -6118566412444005358L;

	private final ConfigurationHandler configurationHandler = ConfigurationHandler.getInstance();

	public WelcomeScreenCheckBoxAction() {
		putValue(NAME, "Show on startup");
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JCheckBox jCheckBox = (JCheckBox) e.getSource();

		configurationHandler.set("SHOW_WELCOME_SCREEN", jCheckBox.isSelected());
	}

}
