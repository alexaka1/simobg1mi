package hu.iit.miskolc.msc.sweng.simob.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.dialogs.FileChooser;
import hu.iit.miskolc.msc.sweng.simob.utils.RoadEditor;

public class SaveProjectAction extends AbstractAction {
	private static final long serialVersionUID = 5483995860576005640L;
	private final FileChooser fileChooser;
	private final RoadEditor roadEditor;

	public SaveProjectAction(FileChooser fileChooser) throws JAXBException {
		super();
		putValue(NAME, "Save");
		roadEditor = RoadEditor.getInstance();
		this.fileChooser = fileChooser;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//Check if Road list is exist to decide if save needed
		if (roadEditor.getRoads() == null)
			return;
		fileChooser.saveProjectFile();

	}

}
