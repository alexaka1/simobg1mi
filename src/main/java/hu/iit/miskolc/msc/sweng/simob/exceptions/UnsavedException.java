package hu.iit.miskolc.msc.sweng.simob.exceptions;

public class UnsavedException extends Exception {

	private static final long serialVersionUID = 234112505919002519L;
	
	public UnsavedException() {
		super();
	}
	
	public UnsavedException(String message) {
		super(message);
	}

}
