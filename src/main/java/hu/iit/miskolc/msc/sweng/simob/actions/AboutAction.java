package hu.iit.miskolc.msc.sweng.simob.actions;

import hu.iit.miskolc.msc.sweng.simob.frames.AboutFrame;
import hu.iit.miskolc.msc.sweng.simob.utils.ConfigurationHandler;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class AboutAction extends AbstractAction {

    private static final long serialVersionUID = -6582086543011887265L;
    private final ConfigurationHandler configurationHandler;

    public AboutAction() {
        putValue(NAME, "About");
        configurationHandler = ConfigurationHandler.getInstance();
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String aboutWindowExist = configurationHandler.get("AboutWindowExist");
        if (aboutWindowExist == null || aboutWindowExist.equals("null")) {
            new AboutFrame();
        }
    }


}
