package hu.iit.miskolc.msc.sweng.simob.frames;

import hu.iit.miskolc.msc.sweng.simob.utils.ConfigurationHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class AboutFrame extends JFrame {
    private static final long serialVersionUID = -5560835294719845333L;

    private static final String TITLE = "About";
    private final ConfigurationHandler configurationHandler;

    public AboutFrame() {
        configurationHandler = ConfigurationHandler.getInstance();
        configurationHandler.set("AboutWindowExist", "asd");
        setTitle(TITLE);
        setSize(500, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        GridLayout experimentLayout = new GridLayout(4, 2);
        setLayout(experimentLayout);

        add(new JLabel("Adam Satan, Team Leader:", JLabel.CENTER));
        add(new JLabel("satan@iit.uni-miskolc.hu", JLabel.CENTER));
        add(new JLabel("Alex Toth:", JLabel.CENTER));
        add(new JLabel("toth130@iit.uni-miskolc.hu", JLabel.CENTER));
        add(new JLabel("Alex Martossy:", JLabel.CENTER));
        add(new JLabel("martossy@iit.uni-miskolc.hu", JLabel.CENTER));
        add(new JLabel("Janos Mate Horvath:", JLabel.CENTER));
        add(new JLabel("horvath23@iit.uni-miskolc.hu", JLabel.CENTER));
        setAlwaysOnTop(true);

        setVisible(true);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                configurationHandler.set("AboutWindowExist", null);
                dispose();
            }
        });

    }
}
