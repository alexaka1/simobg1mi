package hu.iit.miskolc.msc.sweng.simob.dialogs;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.utils.ConfigurationHandler;
import hu.iit.miskolc.msc.sweng.simob.utils.FileExtensions;
import hu.iit.miskolc.msc.sweng.simob.utils.RoadEditor;

public class FileChooser {

	private final ConfigurationHandler configurationHandler;
	private final JFrame frame;
	private JFileChooser fileChooser;
	private File file;
	private final String currentPath;
	private final String defaultFileName;
	private FileNameExtensionFilter fileNameExtensionFilter;
	private final RoadEditor roadEditor;

	public FileChooser(JFrame frame) throws JAXBException {
		super();
		this.fileChooser = new JFileChooser();
		this.configurationHandler = ConfigurationHandler.getInstance();
		this.frame = frame;
		this.currentPath = configurationHandler.get("DEFAULT_PROJECT_PATH");
		this.defaultFileName = this.currentPath + File.separator + configurationHandler.get("DEFAULT_PROJECT_FILENAME");
		this.fileNameExtensionFilter = new FileNameExtensionFilter("xml file", "xml");
		this.roadEditor = RoadEditor.getInstance();

		this.fileChooser.setCurrentDirectory(new File(this.currentPath));

		File saveDir = new File(this.currentPath);
		if (!saveDir.exists()) {
			saveDir.mkdirs();
		}
	}

	/**
	 * Called when clicked on 'New project' button in the menu
	 * As we need a file to save, this will call the save function
	 */
	public void newProjectFile() {
		saveAsNewFile("Select file to use for the project", JFileChooser.SAVE_DIALOG, defaultFileName);
	}

	/**
	 *Called when clicked on 'Open' button in the menu, try to open the project
	 * 
	 */
	public void openProjectFile() {
		openFile("Select file to open", JFileChooser.OPEN_DIALOG, defaultFileName);

	}

	/**
	 * Called when clicked on 'Save' button in the menu
	 * If the file was not saved, then the new project save will be shown
	 * otherwise it will save the project to the selected file
	 */
	public void saveProjectFile() {
		String defVal = "saveName";
		String fileName = configurationHandler.get("fileName", defVal);
		if (fileName.equals(defVal)) {
			newProjectFile();
			return;
		}

		saveFile();
	}

	/**
	 * Called when you clicked on the 'Save as' button in the menu
	 * Will call the function for saving as new project, so it will displays the file chooser
	 */
	public void saveAsProjectFile() {
		saveAsNewFile("Select file to save project into", JFileChooser.SAVE_DIALOG, defaultFileName);
	}

	/**
	 * Called when opening a project
	 * Will show a popup window, where you can select the project you want to open
	 * Will check if the extension is valid, and then will try to load the content from the file
	 * 
	 * @param dialogTitle set the title of the file chooser popup window
	 * @param dialogType 
	 * @param defaultFileName the default filename for the project, set in the configuration file
	 */
	private void openFile(String dialogTitle, int dialogType, String defaultFileName) {
		fileChooser.setDialogTitle(dialogTitle);
		fileChooser.setDialogType(dialogType);
		fileChooser.setSelectedFile(new File(defaultFileName));
		fileChooser.setFileFilter(fileNameExtensionFilter);

		switch (fileChooser.showDialog(frame, null)) {
		case JFileChooser.APPROVE_OPTION:
			file = fileChooser.getSelectedFile();
			//Checks if the extension of the file is valid or not
			boolean isValid = FileExtensions.isFileExtensionXml(file);

			if (isValid && file.exists()) {

				configurationHandler.set("fileName", file.getAbsolutePath());
				try {
					//Try to load the content from the file
					roadEditor.loadRoads();
				} catch (JAXBException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(frame, "Error while parsing the XML.");
				}
			}
			break;

		default:
			break;

		}
	}

	/**
	 * This is used to save the changes in the project, if the project already saved
	 */
	private void saveFile() {
		try {
			roadEditor.saveRoads();
		} catch (JAXBException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(frame, "Error while parsing the XML.");
		}
	}

	/**
	 * This will save the project as a new file
	 * Will display a popup panel, where you can select the destination folder and set the file name
	 * Will call the file saving to the system, and if the saving is successful
	 * it will call the function to save the project itself. 
	 * 
	 * @param dialogTitle set the title of the file chooser popup window
	 * @param dialogType 
	 * @param defaultFileName the default filename for the project, set in the configuration file
	 */
	private void saveAsNewFile(String dialogTitle, int dialogType, String defaultFileName) {
		file = new File(defaultFileName);
		fileChooser.setDialogTitle(dialogTitle);
		fileChooser.setDialogType(dialogType);
		fileChooser.setSelectedFile(file);
		fileChooser.setFileFilter(fileNameExtensionFilter);

		switch (fileChooser.showDialog(frame, null)) {
		case JFileChooser.APPROVE_OPTION:
			file = FileExtensions.setExtensionToXml(fileChooser.getSelectedFile());

			//Call the file creating in the file system
			if (saveToFileSystem(file)) {

				configurationHandler.set("fileName", file.getAbsolutePath());

			} else {
				JOptionPane.showMessageDialog(frame, "Error while saving the file to filesystem.");
			}
			try {
				//Call the save method for the roads
				roadEditor.saveRoads();
			} catch (JAXBException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(frame, "Error while parsing the XML.");
			}
			break;

		default:
			break;
		}
	}

	/**
	 * This will create the file in the file system
	 * Creates the directory and then the file 
	 * 
	 * @param file is the name of the file needed to create in the file system
	 * @return the return type of the function, shows if the creation was successful or not
	 */
	private boolean saveToFileSystem(File file) {

		boolean success = true;

		if (!file.exists()) {
			
			//Creates the directory for the file
			file.getParentFile().mkdirs();

			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				success = false;
			}
		}
		return success;
	}

}
