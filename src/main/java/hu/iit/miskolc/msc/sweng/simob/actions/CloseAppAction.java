package hu.iit.miskolc.msc.sweng.simob.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.dialogs.FileChooser;
import hu.iit.miskolc.msc.sweng.simob.utils.RoadEditor;

public class CloseAppAction extends AbstractAction {

	private static final long serialVersionUID = 4201283435720629276L;
	private FileChooser fileChooser;
	private RoadEditor roadEditor;

	public CloseAppAction(FileChooser fileChooser) throws JAXBException {
		super();
		putValue(NAME, "Exit");
		roadEditor = RoadEditor.getInstance();
		this.fileChooser = fileChooser;

	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		// Checks if the Road list exist to decide if save needed
		if (roadEditor.getRoads() == null)
			System.exit(0);
		
		fileChooser.saveProjectFile();
		System.exit(0);
	}
}
