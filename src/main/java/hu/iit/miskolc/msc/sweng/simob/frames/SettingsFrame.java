package hu.iit.miskolc.msc.sweng.simob.frames;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import hu.iit.miskolc.msc.sweng.simob.panels.RoadEditorPanel;
import hu.iit.miskolc.msc.sweng.simob.utils.ConfigurationHandler;
import hu.iit.miskolc.msc.sweng.simob.utils.NumberTextField;

public class SettingsFrame extends JFrame {

	private static final long serialVersionUID = -2142238006177298651L;
	private static final String TITLE = "Settings";
	private final ConfigurationHandler configurationHandler;
	private RoadEditorPanel roadEditorPanel;
	private JFrame frame;
	private NumberTextField width = new NumberTextField();
	private NumberTextField height = new NumberTextField();
	private NumberTextField min_width = new NumberTextField();
	private NumberTextField min_height = new NumberTextField();

	public SettingsFrame(RoadEditorPanel roadEditorPanel, JFrame frame) {
		configurationHandler = ConfigurationHandler.getInstance();
		configurationHandler.set("SettingsWindowExist", "open");
		this.roadEditorPanel = roadEditorPanel;
		this.frame = frame;
		setTitle(TITLE);
		setSize(500, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		JButton chooser = new JButton("Choose color");
		chooser.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Color newColor = JColorChooser.showDialog(null, "Choose a color", Color.BLACK);
				if (newColor != null)
					setColor(newColor);
			}
		});

		JButton saveSettings = new JButton("Save");
		saveSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveSettings();
				configurationHandler.set("SettingsWindowExist", null);
				dispose();
			}
		});

		GridLayout experimentLayout = new GridLayout(6, 1);
		setLayout(experimentLayout);

		add(new JLabel("Choose color"));
		add(chooser);
		add(new JLabel("Width"));
		width.setText(configurationHandler.get("WIDTH"));
		add(width);
		add(new JLabel("Height"));
		height.setText(configurationHandler.get("HEIGHT"));
		add(height);
		add(new JLabel("Minimum width"));
		min_width.setText(configurationHandler.get("MIN_WIDTH"));
		add(min_width);
		add(new JLabel("Minimum height"));
		min_height.setText(configurationHandler.get("MIN_HEIGHT"));
		add(min_height);
		add(saveSettings);

		setVisible(true);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				configurationHandler.set("SettingsWindowExist", null);
				dispose();
			}
		});

	}

	/**
	 * Set the background color of the drawing panel	 *
	 *  
	 * @param color is the color which will be set for the background
	 */
	private void setColor(Color color) {
		configurationHandler.set("DRAWING_PANEL_BGCOLOR_R", color.getRed());
		configurationHandler.set("DRAWING_PANEL_BGCOLOR_G", color.getGreen());
		configurationHandler.set("DRAWING_PANEL_BGCOLOR_B", color.getBlue());
		roadEditorPanel.updateBackground();
	}

	/**
	 * Called when clicked on the 'Save' button in the Settings window
	 * Saves the values into the configuration file 
	 */
	private void saveSettings() {
		if (min_width.getText().equals("") || min_height.getText().equals("") || width.getText().equals("")
				|| height.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Some of the input fields are empty.");
			return;
		}

		configurationHandler.set("MIN_WIDTH", Integer.parseInt(min_width.getText()));
		configurationHandler.set("MIN_HEIGHT", Integer.parseInt(min_height.getText()));
		configurationHandler.set("WIDTH", Integer.parseInt(width.getText()));
		configurationHandler.set("HEIGHT", Integer.parseInt(height.getText()));
		frame.validate();
	}

}
