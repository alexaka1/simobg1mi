package hu.iit.miskolc.msc.sweng.simob.panels;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.exceptions.UnsavedException;
import hu.iit.miskolc.msc.sweng.simob.model.Road;
import hu.iit.miskolc.msc.sweng.simob.utils.ConfigurationHandler;
import hu.iit.miskolc.msc.sweng.simob.utils.RoadEditor;

public class RoadEditorPanel extends JPanel {

	private static final long serialVersionUID = 4953487201367153202L;

	private final ConfigurationHandler configurationHandler;
	private final RoadEditor roadEditor;

	private RoadListToolbar roadListPanel;
	private RoadDrawingPanel roadDrawingPanel;
	private DrawingPanelControlToolbar drawingControlPanelToolbar;
	private RoadFormToolbar roadForm;
	public boolean drawingEnabled;

	public RoadEditorPanel() throws JAXBException, UnsavedException {
		configurationHandler = ConfigurationHandler.getInstance();
		roadEditor = RoadEditor.getInstance();
		setLayout(new BorderLayout(0, 0));
		roadListPanel = new RoadListToolbar(this);
		roadDrawingPanel = new RoadDrawingPanel(this);
		roadForm = new RoadFormToolbar(this);
		drawingControlPanelToolbar = new DrawingPanelControlToolbar(this);
		drawingEnabled = true;

		add(roadListPanel, BorderLayout.WEST);
		add(drawingControlPanelToolbar, BorderLayout.NORTH);
		add(roadForm, BorderLayout.EAST);
	}

	public void addPanel() {
		add(roadDrawingPanel, BorderLayout.CENTER);
		validate();
	}

	public boolean getDrawingEnabled() {
		return drawingEnabled;
	}

	/**
	 * Set a flag indicating whether drawing is enabled on the panel or not.
	 * 
	 * @param enable Flag indicating whether drawing is enabled on the panel or not.
	 */
	public void setDrawingEnabled(boolean enable) {
		drawingEnabled = enable;
	}

	/**
	 * Clears the drawing panel.
	 */
	public void clearCanvas() {
		if (roadEditor.getRoads() == null)
			return;
		roadDrawingPanel.clearCanvas();
	}

	/**
	 * Undo last change made on drawing panel.
	 */
	public void undoAction() {
		if (roadEditor.getRoads() == null)
			return;
		Road road = roadDrawingPanel.undoAction();
		roadEditor.setRoad(road);
		this.update(this, road);
	}

	/**
	 * Redo a withdrawn change on the drawing panel.
	 */
	public void redoAction() {
		if (roadEditor.getRoads() == null)
			return;
		Road road = roadDrawingPanel.redoAction();
		roadEditor.setRoad(road);
		this.update(this, road);
	}

	/**
	 * Updates components of the editor panel.
	 * 
	 * @param component The component from which the update method was invoked.
	 * @param road      Road object that has to be set on the form.
	 */
	public void update(Object component, Road road) {
		if (component != roadForm) {
			roadForm.updateData(road);
		}
		if (component != roadListPanel) {
			roadListPanel.updateList();
		}
		if (component != roadDrawingPanel) {
			roadDrawingPanel.repaint();
		}
	}

	/**
	 * Updates the drawing panels background color.
	 */
	public void updateBackground() {
		roadDrawingPanel.setBackground();
	}

}
