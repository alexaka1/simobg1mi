package hu.iit.miskolc.msc.sweng.simob.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.dialogs.FileChooser;
import hu.iit.miskolc.msc.sweng.simob.frames.MainFrame;
import hu.iit.miskolc.msc.sweng.simob.panels.RoadEditorPanel;
import hu.iit.miskolc.msc.sweng.simob.utils.RoadEditor;

public class OpenProjectAction extends AbstractAction {
	private static final long serialVersionUID = 7416129765985324469L;
	private final FileChooser fileChooser;
	private final RoadEditor roadEditor;
	private MainFrame frame;
	private RoadEditorPanel roadEditorPanel;

	public OpenProjectAction(RoadEditorPanel roadEditorPanel, FileChooser fileChooser, MainFrame frame)
			throws JAXBException {
		super();
		putValue(NAME, "Open");
		roadEditor = RoadEditor.getInstance();
		this.frame = frame;
		this.fileChooser = fileChooser;
		this.roadEditorPanel = roadEditorPanel;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//Display the panels 
		frame.showPanels();
		fileChooser.openProjectFile();
		roadEditorPanel.addPanel();
		//Check if the Road list has any Road, and set the last Road as selected 
		if (roadEditor.getRoads().size() > 0) {
			roadEditor.setRoad(roadEditor.getRoads().get(roadEditor.getRoads().size() - 1));
		}
		roadEditorPanel.update(this, null);
	}
}
