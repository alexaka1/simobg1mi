package hu.iit.miskolc.msc.sweng.simob.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.dialogs.FileChooser;
import hu.iit.miskolc.msc.sweng.simob.exceptions.UnsavedException;
import hu.iit.miskolc.msc.sweng.simob.frames.MainFrame;
import hu.iit.miskolc.msc.sweng.simob.panels.RoadEditorPanel;
import hu.iit.miskolc.msc.sweng.simob.utils.RoadEditor;

public class NewProjectAction extends AbstractAction {
	private static final long serialVersionUID = 8856332463492189299L;
	private final FileChooser fileChooser;
	private final RoadEditor roadEditor;
	private MainFrame frame;
	private RoadEditorPanel roadEditorPanel;
	
	public NewProjectAction(RoadEditorPanel roadEditorPanel, FileChooser fileChooser, MainFrame frame) throws JAXBException {
		super();
		putValue(NAME, "New project");
		roadEditor = RoadEditor.getInstance();
		this.frame = frame;
		this.roadEditorPanel = roadEditorPanel;
		this.fileChooser = fileChooser;
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//Display the panels, creates the Road list
		frame.showPanels();
		try {
			roadEditor.createRoads();
		} catch (UnsavedException | JAXBException e1) {
			e1.printStackTrace();
		}
		fileChooser.newProjectFile();
		roadEditorPanel.addPanel();
	}
}
