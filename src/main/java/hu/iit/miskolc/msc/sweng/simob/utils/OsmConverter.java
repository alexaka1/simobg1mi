package hu.iit.miskolc.msc.sweng.simob.utils;

import hu.iit.miskolc.msc.sweng.simob.exceptions.UnsavedException;
import hu.iit.miskolc.msc.sweng.simob.model.*;

import javax.xml.bind.JAXBException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OsmConverter {

    public static List<Road> convertToRoadListFromOSM(Osm osm) throws JAXBException, UnsavedException {
        List<Road> roads = RoadEditor.getInstance().createRoads();
        Map<Integer, Point> pointMap = createPointMapFromOSMNodes(osm);
        createRoadListFromOSMNodes(osm, pointMap, roads);
        return roads;
    }

    private static Map<Integer, Point> createPointMapFromOSMNodes(Osm osm) {
        Map<Integer, Point> pointMap = new HashMap<>();
        for (Node node : osm.getNode()) {
            Point point = convertToPointFromCoordinate(node);
            pointMap.put(point.getId(), point);
        }
        return pointMap;
    }

    private static void createRoadListFromOSMNodes(Osm osm, Map<Integer, Point> pointMap, List<Road> roads) {
        Road road = new Road();

        for (Way way : osm.getWay()) {
            for (int i = 0; i < way.getNd().size() - 1; i++) {
                road.setEnd(pointMap.get(way.getNd().get(i).getRef()));
                road.setStart(pointMap.get(way.getNd().get(i + 1)));
                roads.add(road);
            }

        }
    }

    private static Point convertToPointFromCoordinate(Node node) {
        Point point = new Point();
        point.setX(node.getLat());
        point.setY(node.getLon());
        point.setId(Integer.parseInt(node.getId().toString()));
        return point;
    }


}