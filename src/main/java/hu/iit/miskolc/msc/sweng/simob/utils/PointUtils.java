package hu.iit.miskolc.msc.sweng.simob.utils;

import hu.iit.miskolc.msc.sweng.simob.model.Line;
import hu.iit.miskolc.msc.sweng.simob.model.Point;

public class PointUtils {

	/**
	 * Return point's distance from line defined by two other points.
	 * 
	 * @param point
	 * @param lineStart
	 * @param lineEnd
	 * @return Distance of point from the line.
	 */
	public static double distanceFromLine(Point point, Point lineStart, Point lineEnd) {
		if (point == null || lineStart == null || lineEnd == null) {
			return Double.POSITIVE_INFINITY;
		}

		double a = (lineEnd.getY() - lineStart.getY()) * point.getX();
		double b = (lineEnd.getX() - lineStart.getX()) * point.getY();
		double c = lineEnd.getX() * lineStart.getY() - lineEnd.getY() * lineStart.getX();
		double d = Math.abs(a - b + c);

		double e = Math.pow(lineEnd.getY() - lineStart.getY(), 2);
		double f = Math.pow(lineEnd.getX() - lineStart.getX(), 2);
		double g = Math.sqrt(e + f);

		return d / g;
	}

	/**
	 * Check whether the given point is between the start and end point of the line
	 * horizontally.
	 * 
	 * @param point
	 * @param lineStart
	 * @param lineEnd
	 * @return Boolean value indicating whether the given point is between the start
	 *         and end point of the line horizontally.
	 */
	public static boolean isBetweenHorizontally(Point point, Point lineStart, Point lineEnd) {
		if (point == null || lineStart == null || lineEnd == null) {
			return false;
		}
		if (point.getX() >= lineStart.getX() && point.getX() <= lineEnd.getX()) {
			return true;
		}
		if (point.getX() <= lineStart.getX() && point.getX() >= lineEnd.getX()) {
			return true;
		}
		return false;
	}

	/**
	 * Check whether the given point is between the start and end point of the line
	 * vertically.
	 * 
	 * @param point
	 * @param lineStart
	 * @param lineEnd
	 * @return Boolean value indicating whether the given point is between the start
	 *         and end point of the line vertically.
	 */
	public static boolean isBetweenVertically(Point point, Point lineStart, Point lineEnd) {
		if (point == null || lineStart == null || lineEnd == null) {
			return false;
		}
		if (point.getY() >= lineStart.getY() && point.getY() <= lineEnd.getY()) {
			return true;
		}
		if (point.getY() <= lineStart.getY() && point.getY() >= lineEnd.getY()) {
			return true;
		}
		return false;
	}

	/**
	 * Check whether the given point is between the start and end point of the line
	 * both horizontally and vertically.
	 * 
	 * @param point
	 * @param lineStart
	 * @param lineEnd
	 * @return Boolean value indicating whether the given point is between the start
	 *         and end point of the line both horizontally and vertically.
	 */
	public static boolean isBetween(Point point, Point lineStart, Point lineEnd) {
		return isBetweenHorizontally(point, lineStart, lineEnd) && isBetweenVertically(point, lineStart, lineEnd);
	}

	/**
	 * Calculate point of intersection of the given lines.
	 * 
	 * @param line1
	 * @param line2
	 * @return Lines' point of intersection.
	 */
	public static Point getPointOfIntersection(Line line1, Line line2) {
		double x = (line1.getIntercept() - line2.getIntercept()) / (line2.getSlope() - line1.getSlope());
		double y = line1.getSlope() * x + line1.getIntercept();

		return new Point().withX(x).withY(y);
	}

	/**
	 * Calculate point of intersection of the lines defined by the given points.
	 * 
	 * @param lineStart1
	 * @param lineEnd1
	 * @param lineStart2
	 * @param lineEnd2
	 * @return Point of intersection of the lines defined by the given points.
	 */
	public static Point getPointOfIntersection(Point lineStart1, Point lineEnd1, Point lineStart2, Point lineEnd2) {
		if (lineStart1 == null || lineEnd1 == null || lineStart2 == null || lineEnd2 == null) {
			return null;
		}

		Line line1 = getLineEquationFromPoints(lineStart1, lineEnd1);
		Line line2 = getLineEquationFromPoints(lineStart2, lineEnd2);

		return getPointOfIntersection(line1, line2);
	}

	/**
	 * Calculate equation of the line defined by the given points.
	 * 
	 * @param lineStart
	 * @param lineEnd
	 * @return Line equation from two points.
	 */
	public static Line getLineEquationFromPoints(Point lineStart, Point lineEnd) {
		if (lineStart == null || lineEnd == null) {
			return null;
		}

		double slope = (lineEnd.getY() - lineStart.getY()) / (lineEnd.getX() - lineStart.getX());
		double intercept = lineStart.getY() - slope * lineStart.getX();

		return new Line().withSlope(slope).withIntercept(intercept);
	}

}
