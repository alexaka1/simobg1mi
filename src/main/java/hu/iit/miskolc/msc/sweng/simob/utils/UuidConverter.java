package hu.iit.miskolc.msc.sweng.simob.utils;

import java.util.UUID;

public class UuidConverter {

	private UuidConverter() {

	}

	/**
	 * Create UUID from the given string.
	 * 
	 * @param value String value from which UUID must be created.
	 * @return UUID created from the given string value.
	 */
	public static UUID parse(String value) {
		return UUID.fromString(value);
	}

	/**
	 * Print string value of the given UUID.
	 * 
	 * @param value Value of which string representation must be printed.
	 * @return String value of the given UUID.
	 */
	public static String print(UUID value) {
		return value.toString();
	}

}
