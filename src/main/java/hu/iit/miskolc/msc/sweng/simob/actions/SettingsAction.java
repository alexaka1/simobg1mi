package hu.iit.miskolc.msc.sweng.simob.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import hu.iit.miskolc.msc.sweng.simob.frames.SettingsFrame;
import hu.iit.miskolc.msc.sweng.simob.panels.RoadEditorPanel;
import hu.iit.miskolc.msc.sweng.simob.utils.ConfigurationHandler;

public class SettingsAction extends AbstractAction {

	private static final long serialVersionUID = 5746407940288571869L;
	private final ConfigurationHandler configurationHandler;
	private RoadEditorPanel roadEditorPanel;
	private JFrame frame;

    public SettingsAction(RoadEditorPanel roadEditorPanel, JFrame frame) {
        putValue(NAME, "Settings");
        this.roadEditorPanel = roadEditorPanel;
        this.frame = frame;
        configurationHandler = ConfigurationHandler.getInstance();
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String settingsWindowExist = configurationHandler.get("SettingsWindowExist");
        if (settingsWindowExist == null || settingsWindowExist == "null") {
            new SettingsFrame(roadEditorPanel, frame);
        }
    }
}
