package hu.iit.miskolc.msc.sweng.simob.panels;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import hu.iit.miskolc.msc.sweng.simob.actions.WelcomeScreenCheckBoxAction;

public class WelcomeScreen extends JPanel {

	private static final long serialVersionUID = 4560198978585854919L;

	public WelcomeScreen() {
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		addContent();

		addCheckBox();
	}

	/**
	 * Add 'show on startup' checkbox to the panel.
	 */
	private void addCheckBox() {
		JCheckBox jCheckBox = new JCheckBox(new WelcomeScreenCheckBoxAction());
		jCheckBox.setSelected(true);

		add(jCheckBox);
	}

	/**
	 * Add some content to the panel.
	 */
	private void addContent() {
		JTextArea jTextArea = new JTextArea();
		jTextArea.setColumns(80);
		jTextArea.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		jTextArea.setLineWrap(true);
		jTextArea.setRows(30);
		jTextArea.setText(
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent elementum euismod elementum. Duis gravida tempus dictum. Maecenas facilisis est eget purus tincidunt pulvinar. Vivamus egestas ultrices orci, eu pharetra turpis blandit rutrum. Praesent laoreet et dolor vitae rutrum. Ut volutpat, est quis condimentum euismod, enim neque lobortis odio, quis egestas velit lectus nec ante. Etiam maximus bibendum neque, vitae dapibus purus bibendum non. In viverra non ex sit amet commodo. Maecenas molestie sagittis turpis non vestibulum. Mauris efficitur purus at rhoncus auctor. Donec pulvinar elit vel massa vulputate, sit amet interdum ex consectetur. Quisque feugiat pulvinar elit, et bibendum eros molestie a. Fusce et fringilla augue. Praesent pharetra, massa a porttitor auctor, urna quam ultricies diam, et laoreet dui nibh quis arcu.\r\n\r\nAliquam augue purus, sodales ut condimentum sit amet, fringilla non felis. Curabitur vehicula pharetra diam id congue. Donec porta, lorem at fermentum ullamcorper, ante dui porta tellus, vitae convallis tellus urna et augue. Curabitur maximus justo at eleifend vehicula. Aenean quis vestibulum magna. Duis bibendum, nulla nec condimentum tincidunt, nibh mauris blandit quam, sit amet mollis nibh purus ut erat. Fusce id libero vitae metus porta ornare. Suspendisse auctor feugiat lacus, et semper purus tincidunt id. Aliquam in turpis in lacus bibendum viverra. Nulla vel mi et diam pellentesque gravida. Sed maximus, lectus at feugiat condimentum, erat diam dignissim sem, ut tempor arcu ipsum pharetra lorem.\r\n\r\nPraesent eget vehicula arcu. Nam consequat metus euismod, tristique magna ut, sagittis nisl. Nam et arcu tellus. Nunc porta id est a facilisis. Etiam mattis nisi ac magna vulputate, quis sollicitudin eros vehicula. Suspendisse potenti. Nunc efficitur urna vitae neque suscipit, sit amet faucibus odio ultricies.\r\n\r\nSed aliquam nunc vitae blandit tincidunt. Cras auctor, nibh non lobortis sollicitudin, purus est consectetur sapien, in ultricies augue ipsum eget lorem. Donec vestibulum condimentum sapien, in tincidunt velit pretium sit amet. Ut lacus ipsum, dignissim non libero nec, congue consectetur justo. Nunc mauris quam, hendrerit a magna vulputate, egestas molestie elit. Quisque arcu tortor, ullamcorper posuere semper vitae, fringilla a massa. Sed massa quam, iaculis eu pellentesque vulputate, consequat a turpis. Quisque sed odio velit. Duis lobortis, est a mattis dictum, erat nulla dictum augue, at mollis est metus et neque. Fusce convallis malesuada ante non hendrerit. Phasellus venenatis risus eu lacinia elementum. Morbi in malesuada ligula. Suspendisse eu diam nisl. Duis sed vehicula magna.\r\n\r\nSed sagittis, neque pharetra sodales venenatis, justo tellus molestie augue, id efficitur nisl eros vitae orci. Aliquam nec nunc in tellus tincidunt aliquet. Duis nec justo sed arcu ultricies efficitur in id odio. Aliquam ac eleifend est. Nam odio leo, fringilla in aliquam non, egestas a magna. Donec dapibus lorem nisl, viverra dictum nunc bibendum a. Fusce finibus mauris ultricies dui eleifend, in tristique nibh cursus. Cras sit amet lacinia mi, a iaculis quam. Sed ullamcorper aliquet sapien in dictum. In quis luctus nisl.");
		jTextArea.setMaximumSize(new Dimension(1920, 1080));
		jTextArea.setEditable(false);
		jTextArea.setDragEnabled(true);

		add(jTextArea);
	}

}
