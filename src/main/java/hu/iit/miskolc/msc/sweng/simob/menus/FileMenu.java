package hu.iit.miskolc.msc.sweng.simob.menus;

import hu.iit.miskolc.msc.sweng.simob.actions.*;
import hu.iit.miskolc.msc.sweng.simob.dialogs.FileChooser;
import hu.iit.miskolc.msc.sweng.simob.frames.MainFrame;
import hu.iit.miskolc.msc.sweng.simob.panels.RoadEditorPanel;

import javax.swing.*;
import javax.xml.bind.JAXBException;

public class FileMenu extends JMenu {

    private static final long serialVersionUID = -6940830946791460183L;

    public FileMenu(RoadEditorPanel roadEditorPanel, MainFrame frame) {
        setText("File");

        try {
            add(new JMenuItem(new NewProjectAction(roadEditorPanel, new FileChooser(frame), frame)));
            add(new JMenuItem(new OpenProjectAction(roadEditorPanel, new FileChooser(frame), frame)));
            add(new JMenuItem(new SaveProjectAction(new FileChooser(frame))));
            add(new JMenuItem(new SaveAsProjectAction(new FileChooser(frame))));
            add(new JMenuItem(new AboutAction()));
            add(new JMenuItem(new CloseAppAction(new FileChooser(frame))));
        } catch (JAXBException e) {
            // FATAL error
            e.printStackTrace();
        }

        setVisible(true);
    }

}
