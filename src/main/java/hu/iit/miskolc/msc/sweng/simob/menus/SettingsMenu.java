package hu.iit.miskolc.msc.sweng.simob.menus;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.actions.SettingsAction;
import hu.iit.miskolc.msc.sweng.simob.panels.RoadEditorPanel;

public class SettingsMenu extends JMenu {
	private static final long serialVersionUID = -6940830946791460183L;

    public SettingsMenu(RoadEditorPanel roadEditorPanel, JFrame frame) {
        setText("Settings");

        add(new JMenuItem(new SettingsAction(roadEditorPanel, frame)));

        setVisible(true);
    }
}
