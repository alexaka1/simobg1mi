package hu.iit.miskolc.msc.sweng.simob.panels;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JToolBar;

public class DrawingPanelControlToolbar extends JToolBar{

	private static final long serialVersionUID = -3059536268224192639L;

	public DrawingPanelControlToolbar(RoadEditorPanel roadEditorPanel) {
		JRadioButton drawButton = new JRadioButton("Draw");
		drawButton.setSelected(true);
		drawButton.addActionListener((e) -> {
			roadEditorPanel.setDrawingEnabled(true);
			drawButton.setSelected(true);
		});
		roadEditorPanel.setDrawingEnabled(true);
	
		JRadioButton selectButton = new JRadioButton("Select");
		selectButton.addActionListener((e) -> {
			roadEditorPanel.setDrawingEnabled(false);
			selectButton.setSelected(true);
		});
		
		JButton clearButton = new JButton("Clear");
		clearButton.addActionListener((e) -> {
			roadEditorPanel.clearCanvas();
		});
		
		JButton undoButton = new JButton("Undo");
		undoButton.addActionListener((e) -> {
			roadEditorPanel.undoAction();
		});
		
		JButton redoButton = new JButton("Redo");
		redoButton.addActionListener((e) -> {
			roadEditorPanel.redoAction();
		});
	
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(drawButton);
		buttonGroup.add(selectButton);
		buttonGroup.add(clearButton);
		buttonGroup.add(undoButton);
		buttonGroup.add(redoButton);
	
		JPanel radioPanel = new JPanel(new GridLayout(1, 0));
		radioPanel.add(drawButton);
		radioPanel.add(selectButton);
		radioPanel.add(clearButton);
		radioPanel.add(undoButton);
		radioPanel.add(redoButton);
		add(radioPanel, BorderLayout.LINE_START);
	}
}
