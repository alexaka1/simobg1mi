package hu.iit.miskolc.msc.sweng.simob.frames;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.dialogs.FileChooser;
import hu.iit.miskolc.msc.sweng.simob.exceptions.UnsavedException;
import hu.iit.miskolc.msc.sweng.simob.menus.FileMenu;
import hu.iit.miskolc.msc.sweng.simob.menus.SettingsMenu;
import hu.iit.miskolc.msc.sweng.simob.panels.RoadEditorPanel;
import hu.iit.miskolc.msc.sweng.simob.panels.WelcomeScreen;
import hu.iit.miskolc.msc.sweng.simob.utils.ConfigurationHandler;
import hu.iit.miskolc.msc.sweng.simob.utils.RoadEditor;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = -5560835294719845333L;

	private static final String TITLE = "SIMOB";
	private static final int MIN_WIDTH = 800;
	private static final int MIN_HEIGHT = 600;
	private static final int WIDTH = 1024;
	private static final int HEIGHT = 768;
	private FileChooser fileChooser;
	private RoadEditorPanel roadEditorPanel;
	private RoadEditor roadEditor;

	private final ConfigurationHandler configurationHandler;

	public MainFrame() {
		configurationHandler = ConfigurationHandler.getInstance();
		try {
			roadEditorPanel = new RoadEditorPanel();
			roadEditor = RoadEditor.getInstance();
		} catch (JAXBException | UnsavedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			fileChooser = new FileChooser(this);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setUpMenuBar();

		if (configurationHandler.get("SHOW_WELCOME_SCREEN", true)) {
			showWelcomeScreen();
		} else {
			showPanels();
		}

		setMinimumSize(new Dimension(configurationHandler.get("MIN_WIDTH", MIN_WIDTH),
				configurationHandler.get("MIN_HEIGHT", MIN_HEIGHT)));
		setSize(new Dimension(configurationHandler.get("WIDTH", WIDTH), configurationHandler.get("HEIGHT", HEIGHT)));

		setTitle(TITLE);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setVisible(true);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent) {
				if (roadEditor.getRoads() != null)
					fileChooser.saveProjectFile();

				dispose();
			}
		});
	}

	/**
	 * Set up the menubar for the main window
	 * Adds the two menu (FileMenu and SettingsMenu) to the menubar
	 */
	private void setUpMenuBar() {
		JMenuBar menuBar = new JMenuBar();

		menuBar.add(new FileMenu(roadEditorPanel, this));
		menuBar.add(new SettingsMenu(roadEditorPanel, this));

		setJMenuBar(menuBar);
	}

	/**
	 * This function will add the welcome screen to the main frame
	 */
	private void showWelcomeScreen() {
		getContentPane().removeAll();
		getContentPane().add(new WelcomeScreen());
		validate();
	}

	/**
	 * This function will add the roadeditor panel (drawing, roadform, roadlist included) to the main frame
	 */
	public void showPanels() {
		getContentPane().removeAll();
		getContentPane().add(roadEditorPanel);
		validate();
	}
}
