package hu.iit.miskolc.msc.sweng.simob.panels;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.model.Road;
import hu.iit.miskolc.msc.sweng.simob.utils.ConfigurationHandler;
import hu.iit.miskolc.msc.sweng.simob.utils.NumberTextField;
import hu.iit.miskolc.msc.sweng.simob.utils.RoadEditor;

public class RoadFormToolbar extends JToolBar {

	private static final long serialVersionUID = -7168868789604078078L;

	private JTextField nameText = new JTextField();
	private NumberTextField laneText = new NumberTextField();
	private NumberTextField widthText = new NumberTextField();
	private NumberTextField lenghtText = new NumberTextField();
	private JTextField directionText = new JTextField();
	private JButton saveButton = new JButton("Save");
	private static ConfigurationHandler configurationHandler;
	private final RoadEditor roadEditor;
	private RoadEditorPanel roadEditorPanel;

	public RoadFormToolbar(RoadEditorPanel roadEditorPanel) throws JAXBException {
		this.roadEditorPanel = roadEditorPanel;
		roadEditor = RoadEditor.getInstance();
		configurationHandler = ConfigurationHandler.getInstance();

		add(new JLabel("Name: "));
		nameText.setMaximumSize(new Dimension(100, 22));
		add(nameText);
		add(new JLabel("Lanes: "));
		laneText.setMaximumSize(new Dimension(100, 22));
		add(laneText);
		add(new JLabel("Width: "));
		widthText.setMaximumSize(new Dimension(100, 22));
		add(widthText);
		add(new JLabel("Length"));
		lenghtText.setMaximumSize(new Dimension(100, 22));
		add(lenghtText);
		add(new JLabel("Direction"));
		directionText.setMaximumSize(new Dimension(100, 22));
		add(directionText);
		add(new JLabel(""));

		add(saveButton);
		saveButton.addActionListener((e) -> {
			saveData();
		});

		setOrientation(VERTICAL);
	}

	/**
	 * Save changes made on the current road.
	 */
	private void saveData() {
		Road road = roadEditor.getRoad();
		if (road == null)
			return;
		if (directionText.getText().equals("") || lenghtText.getText().equals("") || nameText.getText().equals("")
				|| laneText.getText().equals("") || widthText.getText().equals("")) {
			JOptionPane.showMessageDialog(this, "Some of the input fields are empty.");
			return;
		}
		road.setDirection(directionText.getText());
		road.setLength(Integer.parseInt(lenghtText.getText()));
		road.setName(nameText.getText());
		road.setNumberOfLanes(Integer.parseInt(laneText.getText()));
		road.setWidth(Integer.parseInt(widthText.getText()));
		roadEditorPanel.update(this, road);
	}

	/**
	 * Update form with the given data.
	 * 
	 * @param road Road of which data must be shown in the form.
	 */
	public void updateData(Road road) {
		if (road == null) {
			nameText.setText("");
			laneText.setText("");
			widthText.setText("");
			lenghtText.setText("");
			directionText.setText("");
		} else {
			nameText.setText(road.getName());
			laneText.setText(String.valueOf(road.getNumberOfLanes()));
			widthText.setText(String.valueOf(road.getWidth()));
			lenghtText.setText(String.valueOf(road.getLength()));
			directionText.setText(road.getDirection());
		}
	}

}