package hu.iit.miskolc.msc.sweng.simob.panels;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Stack;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.adapter.MouseListenerAdapter;
import hu.iit.miskolc.msc.sweng.simob.adapter.MouseMotionListenerAdapter;
import hu.iit.miskolc.msc.sweng.simob.exceptions.UnsavedException;
import hu.iit.miskolc.msc.sweng.simob.model.Point;
import hu.iit.miskolc.msc.sweng.simob.model.Road;
import hu.iit.miskolc.msc.sweng.simob.model.Signalization;
import hu.iit.miskolc.msc.sweng.simob.utils.ConfigurationHandler;
import hu.iit.miskolc.msc.sweng.simob.utils.PointUtils;
import hu.iit.miskolc.msc.sweng.simob.utils.RoadEditor;

/**
 * @author Alex
 *
 */
public class RoadDrawingPanel extends JPanel {

	private static final long serialVersionUID = 3339892480440082135L;

	private final ConfigurationHandler configurationHandler;

	private final RoadEditor roadEditor;
	private RoadEditorPanel roadEditorPanel;

	private final Point translate;
	private final Point mousePosition;

	private static final double MIN_SCALE = 0.1;
	private static final double MAX_SCALE = 2.0;
	private static final double DEFAULT_SCALE = 1.0;
	private static final double SCALE_STEP = 0.1;
	private final Point scale;

	private Stack<List<Road>> roadBuffer;

	public RoadDrawingPanel(RoadEditorPanel roadEditorPanel) throws JAXBException, UnsavedException {
		configurationHandler = ConfigurationHandler.getInstance();
		this.roadEditorPanel = roadEditorPanel;
		roadEditor = RoadEditor.getInstance();
		roadBuffer = new Stack<>();

		translate = new Point();
		mousePosition = new Point();

		scale = new Point().withX(DEFAULT_SCALE).withY(DEFAULT_SCALE);

		setBackground();
		setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

		setVisible(true);

		addMouseListener(new MouseListenerAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!SwingUtilities.isLeftMouseButton(e)) {
					return;
				}

				Point end = new Point().withX(e.getX() - translate.getX()).withY(e.getY() - translate.getY());

				if (roadEditorPanel.getDrawingEnabled()) {
					if (roadEditor.getRoad().getStart().equals(end)) {
						List<Road> roads = roadEditor.getRoads();
						roads.remove(roads.size() - 1);
						roadEditor.setRoad(null);
					} else {
						roadEditor.getRoad().setEnd(end);
						roadEditorPanel.update(this, roadEditor.getRoads().get(roadEditor.getRoads().size() - 1));
					}
				} else {
					// select the closest road to the mouse position
					Optional<Road> road = roadEditor.getRoads().stream()
							.filter(r -> PointUtils.isBetweenHorizontally(end, r.getStart(), r.getEnd())
									|| PointUtils.isBetweenVertically(end, r.getStart(), r.getEnd()))
							.sorted((r1, r2) -> {
								double d1 = PointUtils.distanceFromLine(end, r1.getStart(), r1.getEnd());
								double d2 = PointUtils.distanceFromLine(end, r2.getStart(), r2.getEnd());
								return d1 > d2 ? 1 : (d2 > d1 ? -1 : 0);
							}).findFirst();

					if (road.isPresent()) {
						roadEditor.setRoad(road.get());
						roadEditorPanel.update(this, road.get());
					} else {
						roadEditor.setRoad(null);
					}
				}

				repaint();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					mousePosition.setX(e.getX());
					mousePosition.setY(e.getY());
				} else if (SwingUtilities.isLeftMouseButton(e) && roadEditorPanel.getDrawingEnabled()) {
					Road road = roadEditor.createRoad();

					List<Road> roads = roadEditor.getRoads();

					Point start = null;
					if (roads.isEmpty()) {
						start = new Point().withX(e.getX() - translate.getX()).withY(e.getY() - translate.getY());
					} else {
						start = roads.get(roads.size() - 1).getEnd();
					}

					Point end = new Point().withX(start.getX()).withY(start.getY());

					road.setStart(start);
					road.setEnd(end);
					road.setName("{Default}");

					roads.add(road);
					roadEditorPanel.update(this, null);
				}

				repaint();
			}
		});

		addMouseMotionListener(new MouseMotionListenerAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					int mouseMovementX = (int) (e.getX() - mousePosition.getX());
					int mouseMovementY = (int) (e.getY() - mousePosition.getY());

					translate.setX(translate.getX() + mouseMovementX);
					translate.setY(translate.getY() + mouseMovementY);

					mousePosition.setX(e.getX());
					mousePosition.setY(e.getY());
				} else if (SwingUtilities.isLeftMouseButton(e) && roadEditorPanel.getDrawingEnabled()) {
					Point end = new Point().withX(e.getX() - translate.getX()).withY(e.getY() - translate.getY());

					roadEditor.getRoad().setEnd(end);
					roadEditorPanel.update(this, null);
				}
				repaint();
			}
		});

		addMouseWheelListener(e -> {
			double newScale = scale.getX() + Math.signum(e.getWheelRotation()) * SCALE_STEP;

			if (newScale > MIN_SCALE && newScale < MAX_SCALE) {
				scale.setX(newScale);
				scale.setY(newScale);
			}
			repaint();
			roadEditorPanel.update(this, null);
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (scale.getX() != 1.0) {
			g.drawString(String.format("Zoom: %.1fx", 2 - scale.getX()), 5, 15);
		}

		for (Road road : roadEditor.getRoads()) {
			Point start = road.getStart();
			Point end = road.getEnd();

			if (road.equals(roadEditor.getRoad())) {
				g.setColor(Color.RED);
			} else {
				g.setColor(Color.BLACK);
			}

			int startX = (int) ((start.getX() + translate.getX()) / scale.getX());
			int startY = (int) ((start.getY() + translate.getY()) / scale.getY());
			int endX = (int) ((end.getX() + translate.getX()) / scale.getX());
			int endY = (int) ((end.getY() + translate.getY()) / scale.getY());

			g.drawLine(startX, startY, endX, endY);

			for (Signalization signalization : road.getSignalizations()) {
				int signalizationX = (int) ((signalization.getPosition().getX() - translate.getX()) / scale.getX());
				int signalizationY = (int) ((signalization.getPosition().getY() - translate.getY()) / scale.getY());

				g.drawOval(signalizationX, signalizationY, 5, 5);
			}
		}
	}

	/**
	 * Change background color.
	 */
	public void setBackground() {
		int r = configurationHandler.get("DRAWING_PANEL_BGCOLOR_R", 255);
		int g = configurationHandler.get("DRAWING_PANEL_BGCOLOR_G", 255);
		int b = configurationHandler.get("DRAWING_PANEL_BGCOLOR_B", 255);
		setBackground(new Color(r, g, b));
	}

	/**
	 * Clear drawing panel.
	 */
	public void clearCanvas() {
		roadBuffer.push(new ArrayList<>(roadEditor.getRoads()));
		roadEditor.getRoads().clear();
		repaint();
		roadEditorPanel.update(this, null);
	}

	/**
	 * Undo last change.
	 * 
	 * @return Last road.
	 */
	public Road undoAction() {
		if (roadEditor.getRoads().isEmpty())
			return null;

		Road road = roadEditor.getRoads().get(roadEditor.getRoads().size() - 1);
		roadBuffer.push(Arrays.asList(road));
		roadEditor.getRoads().remove(roadEditor.getRoads().size() - 1);
		repaint();
		if (roadEditor.getRoads().size() >= 1)
			return roadEditor.getRoads().get(roadEditor.getRoads().size() - 1);
		return null;
	}

	/**
	 * Redo last withdrawn change.
	 * 
	 * @return Last road.
	 */
	public Road redoAction() {
		if (roadBuffer.isEmpty())
			return null;

		repaint();
		roadEditor.getRoads().addAll(roadBuffer.pop());
		return roadEditor.getRoads().get(roadEditor.getRoads().size() - 1);
	}

}
