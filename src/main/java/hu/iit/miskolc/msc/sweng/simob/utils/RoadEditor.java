package hu.iit.miskolc.msc.sweng.simob.utils;

import java.util.List;

import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.exceptions.UnsavedException;
import hu.iit.miskolc.msc.sweng.simob.model.Road;

public class RoadEditor {

	private static RoadEditor instance = null;
	private final ConfigurationHandler configurationHandler;
	private final RoadService service;
	private List<Road> roads;
	private Road road;

	public RoadEditor() throws JAXBException {
		service = new RoadService();
		configurationHandler = ConfigurationHandler.getInstance();
	}

	/**
	 * Return singleton instance of the RoadEditor class.
	 * 
	 * @return Singleton instance of the RoadEditor class.
	 * @throws JAXBException
	 */
	public static RoadEditor getInstance() throws JAXBException {
		if (instance == null) {
			instance = new RoadEditor();
		}

		return instance;
	}

	/**
	 * Create new road.
	 * 
	 * @return Created road.
	 */
	public Road createRoad() {
		road = service.createRoad();
		return road;
	}

	/**
	 * @return
	 * @throws UnsavedException
	 * @throws JAXBException
	 */
	public List<Road> createRoads() throws UnsavedException, JAXBException {
		if (roads != null) {
			String savedFileName = configurationHandler.get("fileName", "saveName");
			if (savedFileName.equals("saveName")) {
				throw new UnsavedException();
			}
			saveRoads(savedFileName);
		}
		roads = service.createRoads();
		return roads;
	}

	/**
	 * Load road list from the given file.
	 * 
	 * @param fileName File that contains the road list.
	 * @return Road list loaded from the given file.
	 * @throws JAXBException
	 */
	private List<Road> loadRoads(String fileName) throws JAXBException {
		roads = service.load(fileName);
		return roads;
	}

	/**
	 * Load road list from the file stored in config handler.
	 * 
	 * @return Loaded road list.
	 * @throws JAXBException
	 */
	public List<Road> loadRoads() throws JAXBException {
		return loadRoads(configurationHandler.get("fileName"));
	}

	private void saveRoads(String fileName) throws JAXBException {
		service.store(roads, fileName);
	}

	public void saveRoads() throws JAXBException {
		saveRoads(configurationHandler.get("fileName"));
	}

	public List<Road> getRoads() {
		return roads;
	}

	public void setRoad(Road road) {
		this.road = road;
	}

	public Road getRoad() {
		return road;
	}

}
