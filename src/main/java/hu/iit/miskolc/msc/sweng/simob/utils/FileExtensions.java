package hu.iit.miskolc.msc.sweng.simob.utils;

import java.io.File;

import org.apache.commons.io.FilenameUtils;

public class FileExtensions {

	public final static String xml = "xml";

	/**
	 * Set file's extension to .xml.
	 * 
	 * @param file
	 * @return File with .xml extension.
	 */
	public static File setExtensionToXml(File file) {
		if (FilenameUtils.getExtension(file.getName()).equalsIgnoreCase(xml)) {
			// filename is OK as-is
		} else {
			// remove the extension (if any) and replace it with ".xml"
			file = new File(file.getParentFile(), FilenameUtils.getBaseName(file.getName()) + "." + xml);
		}
		return file;
	}

	/**
	 * Check whether the file's extension is .xml
	 * 
	 * @param file
	 * @return Whether the file's extension is .xml
	 */
	public static boolean isFileExtensionXml(File file) {
		if (FilenameUtils.getExtension(file.getName()).equalsIgnoreCase(xml)) {
			return true;
		}
		return false;
	}
}
