package hu.iit.miskolc.msc.sweng.simob.panels;

import java.awt.Dimension;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.xml.bind.JAXBException;

import hu.iit.miskolc.msc.sweng.simob.model.Road;
import hu.iit.miskolc.msc.sweng.simob.utils.RoadEditor;

public class RoadListToolbar extends JToolBar {

	private static final long serialVersionUID = -3107200588138023673L;

	private final RoadEditor roadEditor;
	private RoadEditorPanel roadEditorPanel;

	private DefaultListModel<String> roadListModel;
	private JList<String> roadList;
	private JScrollPane jScrollPane;

	public RoadListToolbar(RoadEditorPanel roadEditorPanel) throws JAXBException {
		this.roadEditorPanel = roadEditorPanel;
		roadEditor = RoadEditor.getInstance();

		roadListModel = new DefaultListModel<>();

		if (roadEditor.getRoads() != null)
			roadEditor.getRoads().forEach(road -> roadListModel.addElement(road.getName()));

		roadList = new JList<>(roadListModel);
		roadList.setSize(new Dimension(150, 400));
		roadList.setLocation(0, 0);
		roadList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		roadList.addListSelectionListener((event) -> {
			if (!event.getValueIsAdjusting()) {
				if (roadList.getSelectedIndex() < 0)
					return;
				Road road = roadEditor.getRoads().get(roadList.getSelectedIndex());
				roadEditor.setRoad(road);
				roadEditorPanel.update(this, road);
			}
		});

		jScrollPane = new JScrollPane(roadList);
		jScrollPane.setSize(150, 400);
		add(jScrollPane);

		setSize(new Dimension(150, 400));
		setOrientation(VERTICAL);
	}

	/**
	 * Update the list of roads.
	 */
	public void updateList() {
		roadList.removeAll();
		roadListModel.clear();

		if (roadEditor.getRoads() != null) {
			roadEditor.getRoads().forEach(road -> roadListModel.addElement(road.getName()));
		}
		roadList.setModel(roadListModel);

	}

}
