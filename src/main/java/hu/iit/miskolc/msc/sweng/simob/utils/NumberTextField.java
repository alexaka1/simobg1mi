package hu.iit.miskolc.msc.sweng.simob.utils;

import java.awt.event.KeyEvent;

import javax.swing.JTextField;

public class NumberTextField extends JTextField {
	private static final long serialVersionUID = 2186723841227410080L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#processKeyEvent(java.awt.event.KeyEvent)
	 */
	@Override
	protected void processKeyEvent(KeyEvent e) {
		if (Character.isDigit(e.getKeyChar()) || e.getKeyChar() == KeyEvent.VK_BACK_SPACE)
			super.processKeyEvent(e);
		e.consume();
		return;
	}

	/**
	 * Get numeric value of the input field.
	 * 
	 * @return Numeric value of the input field.
	 */
	public int getNumber() {
		int result = 0;
		String text = getText();
		if (text != null && !"".equals(text))
			result = Integer.valueOf(text);
		return result;
	}

}
